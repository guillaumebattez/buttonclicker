package fr.guillaumebattez.buttonclicker.views

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import fr.guillaumebattez.buttonclicker.entities.Entity
import com.badlogic.gdx.utils.Array
import fr.guillaumebattez.buttonclicker.common.Assets
import fr.guillaumebattez.buttonclicker.common.Player
import fr.guillaumebattez.buttonclicker.common.Renderer
import fr.guillaumebattez.buttonclicker.extensions.doWithinBounds
import kotlin.properties.Delegates

class ScrollView(var rec: Rectangle) {
    val children = Array<Entity>()
    var scrollVec = 0f
    var scrollPos = 0f
        set(value) {
            field = when {
                value > (sumOfItemsHeight - rec.height) -> sumOfItemsHeight - rec.height
                value < 0 -> 0f
                else -> value
            }
        }
    private val sumOfItemsHeight: Float
        get() = children.map { it.sprite.height }.reduce { sum, itemHeight -> sum + itemHeight }

    private val childrenInRectangle: List<Entity>
        get() = children.filter {
            it.sprite.y + it.sprite.height > rec.y && it.sprite.y < rec.y + rec.height
        }

    var isGrabbed = false

    fun update() {
        if (rec.contains(Player.inputCoordsV2) && Gdx.input.justTouched()) {
            isGrabbed = true
        }
        if (!Gdx.input.isTouched) {
            isGrabbed = false
        }

        if (isGrabbed) {
            scrollVec = -Gdx.input.deltaY.toFloat()
        } else {
            scrollVec *= 0.9f
        }

        scrollPos += scrollVec

        var currentSumOfItemsHeight = 0f
        children.forEach {
            it.sprite.setPosition(rec.x, rec.y + rec.height - it.sprite.height - currentSumOfItemsHeight + scrollPos)
            currentSumOfItemsHeight += it.sprite.height
        }
    }

    fun render() {
        Renderer.batch.color = Color.DARK_GRAY
        Renderer.batch.draw(Assets.Tex.DEBUG(), rec.x, rec.y, rec.width, rec.height)

        Renderer.batch.doWithinBounds(rec) {
            childrenInRectangle.forEach {
                it.render()
            }
        }

        Renderer.batch.color = Color.WHITE
    }
}