package fr.guillaumebattez.buttonclicker.entities

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.Sprite
import fr.guillaumebattez.buttonclicker.common.*
import fr.guillaumebattez.buttonclicker.extensions.endY
import fr.guillaumebattez.buttonclicker.extensions.halfHeight
import fr.guillaumebattez.buttonclicker.screens.IngameScreen
import java.math.BigInteger

class ItemEntity(val itemIndex: Int, val itemType: ItemType) : Entity() {
    var timesBought: Int = 0
    val clickersWithMaxPower: Int
        get() = IngameScreen.clickers.count { it.level == clickerLevelToBuy }
    var clickerLevelToBuy: Int = 0
        get() = (timesBought / IngameScreen.maxMiners).coerceIn(0 until ClickerQuality.values().size)
    val padding = 35
    val purchaseButton = Entity().apply {
        sprite = Sprite(Assets.Tex.DEBUG())
        sprite.color = Color.GOLD
        sprite.setSize(125f, 125f)
        price = BigInteger("50")
        onTouch = {
            itemType.onPurchase(this@ItemEntity)
            timesBought++
            price = when (itemType) {
                ItemType.CLICKER -> ClickerQuality.clickerPrice(clickerLevelToBuy, clickersWithMaxPower)
                ItemType.FINGER -> {
                    var subLevelToBuy = Player.subLevel + 1
                    var levelToBuy = Player.level
                    if (subLevelToBuy > 8) {
                        subLevelToBuy = 1
                        levelToBuy++
                    }
                    ClickerQuality.clickPrice(levelToBuy, subLevelToBuy)
                }
            }
        }
    }

    init {
        sprite.texture = Assets.Tex.BLANK()
        sprite.color = if (itemIndex % 2 == 0) Color(0.55f, 0.65f, 0.7f, 1f) else Color(0.45f, 0.55f, 0.6f, 1f)
        sprite.setSize(Renderer.V_WIDTH, 250f)
    }

    enum class ItemType(var iconTexture: ItemEntity.() -> Texture, var title: String, val onPurchase: ItemEntity.() -> Unit) {
        CLICKER(iconTexture = { ClickerQuality.clickerTextureFromLevel(clickerLevelToBuy) },
                title = "Clicker",
                onPurchase = {
                    val selectedClicker = IngameScreen.clickers[timesBought % IngameScreen.maxMiners]
                    selectedClicker.levelUp()
                    if (selectedClicker.level == 0) {
                        selectedClicker.state = ClickerEntity.State.GotoPlace
                    }
                }),
        FINGER(iconTexture = { ClickerQuality.fingerTextureFromLevel(clickerLevelToBuy) },
                title = "Finger",
                onPurchase = {
                    fr.guillaumebattez.buttonclicker.common.Player.subLevelUp()
                });
    }

    val itemSprite = Sprite(Assets.Tex.CLICKERWHITE()).apply {
     //   setSize(75f, 75f)
    }

    val baseSprite = Sprite(Assets.Tex.CLICKERWHITE()).apply {
        val borderWidth = 20
       // setSize(itemSprite.width - borderWidth, itemSprite.height - borderWidth)
    }

    override fun render() {
        super.render()
        drawIcon()
        drawProgressBar()
        purchaseButton.sprite.setPosition(sprite.x + sprite.width - purchaseButton.sprite.width - padding, sprite.y + sprite.halfHeight - purchaseButton.sprite.halfHeight)
        purchaseButton.render()
    }

    private fun drawIcon() {
        when(itemType) {
            ItemType.CLICKER -> {
                itemSprite.setPosition(sprite.x + padding, sprite.y + sprite.halfHeight - itemSprite.halfHeight)
                itemSprite.color = ClickerQuality.qualityFromLevel(clickerLevelToBuy).outerColor
                itemSprite.draw(Renderer.batch)
                baseSprite.color = ClickerQuality.qualityFromLevel(clickerLevelToBuy).baseColor
                baseSprite.setPosition(sprite.x + padding + 10, sprite.y + sprite.halfHeight - baseSprite.halfHeight)
                baseSprite.draw(Renderer.batch)
            }
            ItemType.FINGER -> {
                itemSprite.texture = Assets.Tex.FINGEROUTERWHITE()
                itemSprite.setPosition(sprite.x + padding, sprite.y + sprite.halfHeight - itemSprite.halfHeight)
                itemSprite.color = ClickerQuality.qualityFromLevel(clickerLevelToBuy).outerColor
                itemSprite.draw(Renderer.batch)
                baseSprite.texture = Assets.Tex.FINGERBASEWHITE()
                baseSprite.color = ClickerQuality.qualityFromLevel(clickerLevelToBuy).baseColor
                baseSprite.setPosition(sprite.x + padding, sprite.y + sprite.halfHeight - baseSprite.halfHeight)
                baseSprite.draw(Renderer.batch)
            }
        }
    }

    private fun drawProgressBar() {
        val progressBarWidth = ((sprite.width - itemSprite.x - itemSprite.width - purchaseButton.sprite.width - padding - padding * 2) / IngameScreen.maxMiners)
        val progressBarHeight = 60f

        repeat(IngameScreen.maxMiners) {
            Renderer.batch.color = Color.DARK_GRAY
            Renderer.batch.draw(Assets.Tex.DEBUG(), itemSprite.x + padding + itemSprite.width + it * progressBarWidth, sprite.y + sprite.halfHeight - progressBarHeight / 2, progressBarWidth, progressBarHeight)
            Renderer.batch.color = Color.WHITE
        }

        when (itemType) {
            ItemType.CLICKER -> {
                val progress = when {
                    timesBought < IngameScreen.maxMiners -> clickersWithMaxPower
                    IngameScreen.clickers.all { it.level == IngameScreen.clickers[0].level } -> IngameScreen.maxMiners
                    clickersWithMaxPower > 0 -> clickersWithMaxPower
                    else -> 0
                }

                repeat(progress) {
                    Renderer.batch.color = Color.LIME
                    Renderer.batch.draw(Assets.Tex.DEBUG(), itemSprite.x + padding + itemSprite.width + it * progressBarWidth, sprite.y + sprite.halfHeight - progressBarHeight / 2, progressBarWidth, progressBarHeight)
                    Renderer.batch.color = Color.WHITE


                }
                Font.itemTitle.draw(Renderer.batch, "${ClickerQuality.qualityFromLevel(clickerLevelToBuy)} ${itemType.title}", sprite.x + padding * 2 + itemSprite.width, sprite.endY - 26)
            }

            ItemType.FINGER -> {
                repeat(Player.subLevel) {
                    Renderer.batch.color = Color.LIME
                    Renderer.batch.draw(Assets.Tex.DEBUG(), itemSprite.x + padding + itemSprite.width + it * progressBarWidth, sprite.y + sprite.halfHeight - progressBarHeight / 2, progressBarWidth, progressBarHeight)
                    Renderer.batch.color = Color.WHITE
                }
                Font.itemTitle.draw(Renderer.batch, "${ClickerQuality.qualityFromLevel(clickerLevelToBuy)} ${itemType.title}", sprite.x + padding * 2 + itemSprite.width, sprite.endY - 26)
            }
        }
    }
}