package fr.guillaumebattez.buttonclicker.entities

import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Vector2
import fr.guillaumebattez.buttonclicker.common.Assets
import fr.guillaumebattez.buttonclicker.common.Font
import fr.guillaumebattez.buttonclicker.common.Player
import fr.guillaumebattez.buttonclicker.common.Renderer
import fr.guillaumebattez.buttonclicker.extensions.halfHeight
import fr.guillaumebattez.buttonclicker.extensions.halfWidth
import fr.guillaumebattez.buttonclicker.extensions.justTouched
import fr.guillaumebattez.buttonclicker.extensions.toCompactString
import fr.guillaumebattez.buttonclicker.screens.IngameScreen
import java.math.BigInteger

open class Entity {
    var sprite: Sprite = Sprite(Assets.Tex.DEBUG())
    var onTouch: () -> Unit = {}
    var onUpdate: () -> Unit = {}
    var price: BigInteger = BigInteger.ZERO
    var ticks: Float = 0f
    var drawPriority = 100
    val p: Vector2 = Vector2()
        get() {
            field.x = sprite.x
            field.y = sprite.y
            return field
        }
    val center: Vector2 = Vector2()
        get() {
            field.x = sprite.x + sprite.halfWidth
            field.y = sprite.y + sprite.halfHeight
            return field
        }

    open fun update() {
        if (sprite.justTouched && price <= Player.score) {
            Player.score -= price
            onTouch()
        }

        updatePrice()
        onUpdate()
        ticks += 1f
    }

    open fun updatePrice() {
    }

    open fun render() {
        update()
        sprite.draw(Renderer.batch)
        if (price > BigInteger.ZERO) {
            Font.drawString(batch = Renderer.batch,
                    rec = sprite.boundingRectangle,
                    textAlign = Font.TextAlign.CENTER,
                    text = price.toCompactString(4),
                    font = Font.fatText,
                    truncate = false)
        }
    }

    fun spawnEntity(insertAtIndexZero: Boolean = false) {
        if (insertAtIndexZero) {
            IngameScreen.entities.insert(0, this)
        } else {
            IngameScreen.entities.add(this)
        }
    }

    fun deleteEntity() {
        IngameScreen.entities.removeValue(this, false)
    }
}