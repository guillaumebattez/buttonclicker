package fr.guillaumebattez.buttonclicker.entities

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import fr.guillaumebattez.buttonclicker.common.Assets
import fr.guillaumebattez.buttonclicker.common.Font
import fr.guillaumebattez.buttonclicker.common.Renderer
import fr.guillaumebattez.buttonclicker.extensions.angleFrom
import fr.guillaumebattez.buttonclicker.extensions.endX
import fr.guillaumebattez.buttonclicker.screens.IngameScreen

class FloatingTextEntity(val text: String, val startingPos: Vector2, val style: Style = Style.ANGLEFROMMINE): Entity() {
    enum class Style { ANGLEFROMMINE, NORMAL, LEVELUP }

    var vel : Vector2
    private var rotationSpeed = MathUtils.random(6f)
    val totalWidth
        get() = Font.fatText.spaceWidth*text.length + sprite.width

    init {
        sprite = Sprite(Assets.Tex.GOLDNUGGET())
        sprite.setSize(60f, 40f)
        sprite.setPosition(startingPos.x - totalWidth/2, startingPos.y)
        sprite.setOriginCenter()
        vel = Vector2(1f, 0f).apply {
            when (style) {
                Style.ANGLEFROMMINE -> setAngle(startingPos.angleFrom(IngameScreen.goldMine.center)).rotate(MathUtils.random(-12f, 12f)).scl(9f)
                Style.LEVELUP, Style.NORMAL -> setAngle(90f).rotate(MathUtils.random(-12f, 12f)).scl(5f)
            }
        }
    }

    override fun update() {
        super.update()
        sprite.x = (sprite.x + vel.x).coerceAtMost(Renderer.V_WIDTH - totalWidth).coerceAtLeast(0f)
        sprite.y = (sprite.y + vel.y).coerceAtMost(Renderer.V_HEIGHT - sprite.height).coerceAtLeast(0f)
        when (style) {
            Style.NORMAL, Style.ANGLEFROMMINE -> {
                vel.scl(0.93f)
                if (ticks > 25f) {
                    sprite.setAlpha((sprite.color.a - 0.045f).coerceAtLeast(0f))
                }
            }
            Style.LEVELUP -> {
                vel.scl(0.98f)
                if (ticks > 100f) {
                    sprite.setAlpha((sprite.color.a - 0.02f).coerceAtLeast(0f))
                }
            }
        }
        rotationSpeed *= 0.98f
        sprite.rotate(rotationSpeed)


        if (sprite.color.a <= 0f) {
            deleteEntity()
        }
    }

    override fun render() {
        super.render()
        when (style) {
            Style.ANGLEFROMMINE, Style.NORMAL -> Font.fatTextSmall.draw(Renderer.batch, text, sprite.endX + 5, sprite.y + Font.fatText.lineHeight/2)
            Style.LEVELUP ->  Font.fatText.draw(Renderer.batch, text, sprite.endX + 5, sprite.y + Font.fatText.lineHeight/2)
        }
    }
}