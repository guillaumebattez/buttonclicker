package fr.guillaumebattez.buttonclicker.entities

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Vector2
import fr.guillaumebattez.buttonclicker.common.Assets


class ShockwaveEntity(val pX: Float, val pY: Float, val targetSize: Float, col: Color = Color.WHITE): Entity() {
    init {
        sprite = Sprite(Assets.Tex.SHOCKWAVE()).apply {
            setSize(20f, 20f)
            setAlpha(0.5f)
            color = col
        }
        drawPriority = 50
    }

    override fun update() {
        super.update()
        with(sprite) {
            setAlpha((color.a - 0.02f).coerceAtLeast(0f))
            setSize(com.badlogic.gdx.math.MathUtils.lerp(width, targetSize, 0.3f), com.badlogic.gdx.math.MathUtils.lerp(height, targetSize, 0.3f))
            setCenter(pX, pY)

            if (color.a <= 0) {
                deleteEntity()
            }
        }
    }
}