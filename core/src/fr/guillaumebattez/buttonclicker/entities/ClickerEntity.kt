package fr.guillaumebattez.buttonclicker.entities

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import fr.guillaumebattez.buttonclicker.common.*
import fr.guillaumebattez.buttonclicker.extensions.*
import fr.guillaumebattez.buttonclicker.screens.IngameScreen
import java.math.BigInteger
import kotlin.coroutines.experimental.CoroutineContext

class ClickerEntity(var launchID: Float) : Entity(), Leveling {
    override var subLevel: Int = 1
    var startingPos = Vector2()
    val posWhenHit = Vector2()
    val clickPower: BigInteger
        get() = ClickerQuality.basePowerFromLevel(level.coerceAtLeast(0))

    val headPos = Vector2()
    val defaultPos = Vector2(Renderer.V_WIDTH/2, IngameScreen.scrollViewHeight - sprite.height)
    var maskAlpha = 0f
    val mask = Sprite(Assets.Tex.MASKCLICKER())
    val baseSprite = Sprite(Assets.Tex.CLICKERWHITE())
        get() {
            val borderWidth = 20
            field.setPosition(sprite.x + borderWidth/2, sprite.y + borderWidth/2)
            field.setSize(sprite.width - borderWidth, sprite.height - borderWidth)
            field.setOriginCenter()
            field.rotation = sprite.rotation
            field.color = ClickerQuality.qualityFromLevel(level).baseColor
            return field
        }

    enum class State { Idle, GotoPlace, Attacking, Reloading }

    override var level = -1
        set(value) {
            ShockwaveEntity(pX = center.x, pY = center.y, targetSize = 200f, col = LIGHT_BLUE).spawnEntity()
         //   sprite.texture = ClickerQuality.clickerTextureFromLevel(value)
            if (value > 1) {
                Assets.Snd.LEVELUP().play(0.7f, 1.8f, 0f)
            }
            sprite.color = ClickerQuality.qualityFromLevel(value).outerColor
            maskAlpha = 1f
            field = value
        }
    var state = State.Reloading
    var hasAttacked = false

    val posInterpolator = Interpolator()

    init {
        sprite = Sprite(Assets.Tex.CLICKERWHITE()).apply {
            setPosition(Renderer.V_WIDTH / 2, 0f)
            setSize(width * 0.75f, height * 0.75f)
            setOriginCenter()
        }
    }

    override fun update() {
        super.update()
        val attackingP = IngameScreen.goldMine.center
        if (IngameScreen.clickerToLaunch > launchID && !hasAttacked && state != State.GotoPlace) {
            state = State.Attacking
            posInterpolator.reset()
            hasAttacked = true
        }
        if (level == -1) {
            state = State.Idle
        }

        if (maskAlpha > 0) {
            maskAlpha = (maskAlpha - 0.04f).coerceAtLeast(0f)
            mask.setPosition(sprite.x, sprite.y)
            mask.setSize(sprite.width, sprite.height)
            mask.setOriginCenter()
            mask.rotation = sprite.rotation
            mask.color = Color(0.6f, 0.9f, 1f, maskAlpha)
        }

        when (state) {
            State.Idle -> {}
            State.GotoPlace -> {
                headPos.set(posInterpolator.apply(defaultPos, startingPos, 0.8f, Interpolation.pow5))
                sprite.setPosition(headPos.x - sprite.width / 2, headPos.y - sprite.height / 2)
                if (headPos.dst(startingPos) < 10f) {
                    state = State.Reloading
                }
            }
            State.Attacking -> {
                headPos.set(posInterpolator.apply(startingPos, attackingP, 0.45f, Interpolation.swingIn))
                sprite.setPosition(headPos.x - sprite.width / 2, headPos.y - sprite.height / 2)
                if (headPos.dst(IngameScreen.goldMine.originP) < IngameScreen.goldMine.sprite.width / 2 + sprite.width / 2) {
                    state = State.Reloading
                    //VECTOR2 each FRAME: NOT GOOD
                    IngameScreen.goldMine.onHit(clickPower, Vector2(headPos))
                    posInterpolator.reset()
                    posWhenHit.x = headPos.x
                    posWhenHit.y = headPos.y
                }
            }
            State.Reloading -> {
                headPos.set(posInterpolator.apply(posWhenHit, startingPos, 0.5f, Interpolation.pow5Out))
                sprite.setPosition(headPos.x - sprite.width / 2, headPos.y - sprite.height / 2)
            }
        }
        sprite.rotation = headPos.angleFrom(IngameScreen.goldMine.center) + 90
    }

    override fun render() {
        super.render()
        baseSprite.draw(Renderer.batch)
        if (maskAlpha > 0) {
            mask.draw(Renderer.batch)
        }
    }
}