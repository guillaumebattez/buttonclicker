package fr.guillaumebattez.buttonclicker.entities

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.math.Vector2
import fr.guillaumebattez.buttonclicker.common.Assets
import fr.guillaumebattez.buttonclicker.common.Font
import fr.guillaumebattez.buttonclicker.common.Player
import fr.guillaumebattez.buttonclicker.common.Renderer
import fr.guillaumebattez.buttonclicker.extensions.*
import fr.guillaumebattez.buttonclicker.screens.IngameScreen
import java.math.BigDecimal
import java.math.BigInteger
import kotlin.properties.Delegates

class GoldEntity : Entity() {
    var levelupFlashAlpha = 0f
    var level: BigInteger by Delegates.observable(BigInteger.ONE) {_, oldValue, newValue ->
        if (newValue <= oldValue) return@observable
        ShockwaveEntity(center.x, center.y, 1000f, col = LIGHT_BLUE).spawnEntity()
        sprite.color = Color(0.2f, 0.7f, 1f, 1f)
        Assets.Snd.LEVELUP().play()
        levelupFlashAlpha = 0.6f
        Player.earnGold(requiredToLevelUp.div(BigInteger("8")), center.apply { y += 50 }, style = FloatingTextEntity.Style.LEVELUP)
    }

    var levelProgress: BigInteger = BigInteger.ZERO
        set(value) {
            field = value
            if (field >= requiredToLevelUp) {
                prevRequiredToLevelUp = requiredToLevelUp
                levelProgressPercent = 1f
                level++
                field = BigInteger.ZERO
            }
        }

    var levelProgressPercent = 0f
        get() {
            field = MathUtils.lerp(field, (BigDecimal("$levelProgress.00")/BigDecimal("$requiredToLevelUp.00")).toFloat(), 0.2f)
            return field
        }

    var prevRequiredToLevelUp: BigInteger = BigInteger.ZERO

    val requiredToLevelUp: BigInteger
        get() = BigDecimal("1.23").pow(level.toInt() + 25).toBigInteger().add(BigInteger("783"))

    val levelStringRec = Rectangle(p.x, p.y, sprite.width, sprite.height + Font.fatTextSmall.lineHeight)
    get() {
        field.x = p.x
        field.y = p.y
        field.width = sprite.width
        field.height = sprite.height + Font.fatTextSmall.lineHeight
        return field
    }

    val originP = Vector2()

    init {
        sprite = Sprite(Assets.Tex.CIRCLE()).apply {
            color = Color.ORANGE
            setSize(350f, 350f)
            setPosition(Renderer.V_WIDTH / 2 - halfWidth, Renderer.V_HEIGHT / 2 - halfHeight + 222)
            originP.x = x + halfWidth
            originP.y = y + halfHeight
        }
    }

    fun onHit(clickPower: BigInteger, pos: Vector2, isFromPlayer: Boolean = false) {
      //  sprite.x += MathUtils.random(-3f, 3f)
        //sprite.y += MathUtils.random(-3f, 3f)
        Assets.Snd.MININGHIT().stop()
        Assets.Snd.MININGHIT().play()
        IngameScreen.entities.add(HitmarkerEntity(pos.x, pos.y))
        Player.earnGold(clickPower, pos)
        levelProgress += clickPower
     //   sprite.color = Color.ORANGE
    }

    override fun update() {
        super.update()
        with(sprite) {
            color = color.lerp(1.1f, 0.4f, 0.1f, 1f, 0.04f)
            x += 0.1f * (originP.x - halfWidth - x)
            y += 0.1f * (originP.y - halfHeight - y)
        }
        levelupFlashAlpha = (levelupFlashAlpha - 0.05f).coerceAtLeast(0f)
    }

    val progressRectangle = Rectangle()
        get() {
            field.width = sprite.width*0.6f
            field.x = p.x + field.width/3
            field.y = p.y + sprite.halfHeight - 5
            field.height = -50f
            return field
        }

    override fun render() {
        super.render()
        Font.drawString(batch = Renderer.batch,
                truncate = false,
                font = Font.fatTextSmall,
                text = "Lvl ${level.toCompactString(4)}",
                textAlign = Font.TextAlign.CENTER,
                rec = levelStringRec)

        val bgColor = Color(0.1f, 0.3f, 0.4f, 1f)
        val borderColor = Color(0.05f, 0.15f, 0.2f, 1f)
        val fillColor = Color(0.2f, 0.7f, 1f, 1f)

        //border
        Renderer.batch.draw(Assets.Tex.BLANK(), progressRectangle.x, progressRectangle.y, progressRectangle.width, progressRectangle.height, borderColor)
        //bg
        Renderer.batch.draw(Assets.Tex.BLANK(), progressRectangle.x + 3, progressRectangle.y - 3, progressRectangle.width - 8, progressRectangle.height + 8, bgColor)
        //fill
        Renderer.batch.draw(Assets.Tex.BLANK(), progressRectangle.x + 3, progressRectangle.y - 3, (progressRectangle.width - 8)*levelProgressPercent, progressRectangle.height + 8, fillColor)
        //text
        Font.drawString(batch = Renderer.batch,
                truncate = false,
                font = Font.textTiny,
                text = if (levelProgress == BigInteger.ZERO && level > BigInteger.ZERO) {
                    "${prevRequiredToLevelUp.toCompactString(4)} / ${prevRequiredToLevelUp.toCompactString(4)}"
                } else {
                    "${levelProgress.toCompactString(4)} / ${requiredToLevelUp.toCompactString(4)}"
                },
                textAlign = Font.TextAlign.CENTER,
                rec = progressRectangle)
        //flash
        Renderer.batch.draw(Assets.Tex.BLANK(), progressRectangle.x, progressRectangle.y, progressRectangle.width, progressRectangle.height, Color(1f, 1f, 1f, levelupFlashAlpha))

    }
}