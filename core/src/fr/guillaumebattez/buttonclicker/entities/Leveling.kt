package fr.guillaumebattez.buttonclicker.entities

interface Leveling {
    var level: Int
    var subLevel: Int

    fun maxSubLevels() = 8
    fun levelUp() {
        level++
    }
    fun subLevelUp() {
        if (++subLevel > maxSubLevels()) {
            subLevel = 1
            levelUp()
        }
    }
}