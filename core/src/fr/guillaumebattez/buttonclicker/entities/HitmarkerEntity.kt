package fr.guillaumebattez.buttonclicker.entities

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import fr.guillaumebattez.buttonclicker.common.Assets
import fr.guillaumebattez.buttonclicker.common.Renderer
import fr.guillaumebattez.buttonclicker.extensions.halfHeight
import fr.guillaumebattez.buttonclicker.extensions.halfWidth

class HitmarkerEntity(x: Float, y: Float): Entity() {
    var shockwaveAlpha = 0.5f

    val shockwaveSprite = Sprite(Assets.Tex.SHOCKWAVE()).apply {
        setPosition(x - halfWidth, y - halfHeight)
        setSize(20f, 20f)
        setAlpha(0.3f)
        drawPriority = 50
    }

    init {
        sprite = Sprite(Assets.Tex.HITMARKER())
        sprite.setSize(60f, 60f)
        sprite.setPosition(x - sprite.halfWidth, y - sprite.halfHeight)
        sprite.setOriginCenter()
        sprite.rotation = MathUtils.random(360f)
        sprite.setAlpha(0.6f)
        repeat(3) {
            SparkEntity(x, y).spawnEntity()
        }
    }

    override fun update() {
        super.update()
        sprite.setAlpha((sprite.color.a - 0.03f).coerceAtLeast(0f))
        with(shockwaveSprite) {
            setAlpha((color.a - 0.02f).coerceAtLeast(0f))
            setSize(MathUtils.lerp(width, 260f, 0.3f), MathUtils.lerp(height, 260f, 0.3f))
            setPosition(sprite.x - halfWidth + sprite.halfWidth, sprite.y - halfHeight + sprite.halfHeight)
        }
        if (ticks > 60) {
            deleteEntity()
        }
    }

    override fun render() {
        super.render()
        shockwaveSprite.draw(Renderer.batch)
    }

    class SparkEntity(x: Float, y: Float): Entity() {
        private val vel = Vector2(1f, 0f).apply { setToRandomDirection().scl(MathUtils.random(11f, 41f)) }

        init {
            sprite = Sprite(Assets.Tex.BLANK())
            sprite.setPosition(x, y)
            sprite.color = Color.GOLD
            sprite.rotation = vel.angle()
        }

        override fun update() {
            super.update()
            sprite.x += vel.x
            sprite.y += vel.y
            sprite.setSize(vel.x*3, 8f)
            vel.scl(0.76f)
            if (vel.len() < 0.1f) {
                deleteEntity()
            }
        }
    }
}