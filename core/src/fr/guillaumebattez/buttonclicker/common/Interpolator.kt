package fr.guillaumebattez.buttonclicker.common

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.math.Interpolation
import com.badlogic.gdx.math.Vector2

class Interpolator {
    var elapsed: Float = 0f

    var onEnd: () -> Unit = {}
    var canTriggerOnEnd = true
    private val v = Vector2()

    fun apply(from: Float, to: Float, interpolationTime: Float, interpolation: Interpolation): Float {
        elapsed += Gdx.graphics.deltaTime

        val progress = (elapsed / interpolationTime).coerceAtMost(1f)
        val alpha = interpolation.apply(progress)

        if (canTriggerOnEnd && progress >= 1f) {
            onEnd()
            canTriggerOnEnd = false
        }
        return from + (to - from) * alpha
    }

    fun apply(from: Vector2, to: Vector2, interpolationTime: Float, interpolation: Interpolation): Vector2 {
        elapsed += Gdx.graphics.deltaTime

        val progress = (elapsed / interpolationTime).coerceAtMost(1f)
        val alpha = interpolation.apply(progress)

        if (canTriggerOnEnd && progress >= 1f) {
            onEnd()
            canTriggerOnEnd = false
        }

        v.x = from.x + (to.x - from.x) * alpha
        v.y = from.y + (to.y - from.y) * alpha
        return v
    }

    fun reset() {
        elapsed = 0f
        canTriggerOnEnd = true
    }
}