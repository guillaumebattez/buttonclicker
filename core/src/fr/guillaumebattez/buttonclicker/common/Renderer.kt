package fr.guillaumebattez.buttonclicker.common

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.OrthographicCamera
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.graphics.glutils.ShapeRenderer
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.Stage
import com.badlogic.gdx.utils.viewport.FitViewport
import com.badlogic.gdx.utils.viewport.StretchViewport

object Renderer {
    //for scissors
    val recZero = Rectangle()
    val V_WIDTH = 720f
    val V_HEIGHT = 1480f
    var camera = OrthographicCamera().apply { translate(V_WIDTH/2 + 2000, V_HEIGHT/2); zoom = 1f }
    val batch = SpriteBatch()
    val hudBatch = SpriteBatch()
    val backgroundBatch = SpriteBatch()
    val shapeRenderer = ShapeRenderer()
    val viewport = StretchViewport(V_WIDTH, V_HEIGHT, camera)
    private val stage = Stage(viewport)
    var superJumpAnimProgress = 0f
}