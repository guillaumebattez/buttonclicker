package fr.guillaumebattez.buttonclicker.common

import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import fr.guillaumebattez.buttonclicker.extensions.*
import sun.plugin2.util.ColorUtil
import java.math.BigInteger
import kotlin.math.roundToLong

enum class ClickerQuality(val basePower: BigInteger,
                          val baseColor: Color,
                          val outerColor: Color,
                          val clickerTexture: Texture = Assets.Tex.CLICKERWHITE()) {
    PAPER(basePower = BigInteger("8"), baseColor = PAPER_BASE, outerColor = PAPER_OUTLINE),
    PLASTIC(basePower = BigInteger("32"), baseColor = Color.GREEN, outerColor = Color.LIME),
    WOOD(basePower = BigInteger("64"), baseColor = WOOD_BASE, outerColor = WOOD_OUTLINE),
    STONE(basePower = BigInteger("500"), baseColor = STONE_BASE, outerColor = STONE_OUTLINE),
    IRON(basePower = BigInteger("1000"), baseColor = IRON_BASE, outerColor = IRON_OUTLINE),
    GOLD(basePower = BigInteger("9999"), baseColor = GOLD_BASE, outerColor = GOLD_OUTLINE),
    DIAMOND(basePower = BigInteger("100000"), baseColor = DIAMOND_BASE, outerColor = DIAMOND_OUTLINE),
    REFINED_DIAMOND(basePower = BigInteger("1500000"), baseColor = REFINED_DIAMOND_BASE, outerColor = REFINED_DIAMOND_OUTLINE),
    FIRE(basePower = BigInteger("666666666"), baseColor = FIRE_BASE, outerColor = FIRE_OUTLINE),
    LASER(basePower = BigInteger("57" e 10), baseColor = Color.RED, outerColor = Color.CORAL),
    ULTRALASER(basePower = BigInteger("10" e 12), baseColor = Color.BLUE, outerColor = Color.ROYAL);

    companion object {
        fun clickerTextureFromLevel(level: Int) = Assets.Tex.valueOf("${qualityFromLevel(level).name}clicker".toUpperCase()).invoke()
        fun fingerTextureFromLevel(level: Int) = Assets.Tex.valueOf("${qualityFromLevel(level).name}finger".toUpperCase()).invoke()
        fun qualityFromLevel(level: Int) = values().first { it.ordinal == level.coerceIn(0 until ClickerQuality.values().size) }
        fun basePowerFromLevel(level: Int): BigInteger = qualityFromLevel(level).basePower
        fun powerFromLevel(level: Int, subLevel: Int): BigInteger = (basePowerFromLevel(level) * ((subLevel - 1) * 0.1429 + 1))
        fun clickPrice(level: Int, subLevel: Int) = powerFromLevel(level, subLevel).times(BigInteger("80"))
        fun clickerPrice(level: Int, clickerNumber: Int): BigInteger = (basePowerFromLevel(level) * BigInteger("20") * (1 + clickerNumber.toDouble() / 16))
    }
}