package fr.guillaumebattez.buttonclicker.common

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Batch
import com.badlogic.gdx.graphics.g2d.BitmapFont
import com.badlogic.gdx.graphics.g2d.GlyphLayout
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator
import com.badlogic.gdx.math.Rectangle

/** Singleton qui initialise les fonts, et fournis des fonctions pour l'écriture de text */
object Font {

    val fontScore: BitmapFont = newFont("fonts/Century Gothic Regular.ttf") {
        size = 128
        borderWidth = 12f
        color = Color.WHITE
        borderColor = Color.BLACK
    }

    val fontText: BitmapFont = newFont("fonts/Century Gothic Regular.ttf") {
        size = 32
        borderWidth = 0f
        color = Color.BLACK
        borderColor = Color.WHITE
    }

    val fatText: BitmapFont = newFont("fonts/Quicksand-Medium.ttf") {
        size = 64
        borderWidth = 5f
        color = Color.WHITE
        borderColor = Color.BLACK
    }

    val itemTitle: BitmapFont = newFont("fonts/Quicksand-Bold.ttf") {
        size = 36
        color = Color(0.2f, 0.3f, 0.45f, 1f)
        borderColor = Color(Color.BLACK)
    }

    val fatTextSmall: BitmapFont = newFont("fonts/Quicksand-Bold.ttf") {
        size = 32
        borderWidth = 3f
        color = Color.WHITE
        borderColor = Color.BLACK
    }

    val textTiny: BitmapFont = newFont("fonts/Quicksand-Bold.ttf") {
        size = 28
        color = Color.WHITE
    }

    /** Enumeration contenant les alignements de textes possibles */
    enum class TextAlign {
        UPLEFT, UP, UPRIGHT, CENTERLEFT, CENTER, CENTERRIGHT, BOTTOMLEFT, BOTTOM, BOTTOMRIGHT
    }

    /**
     * Ecrit du texte dans un rectangle selon un alignement
     *
     * @param batch Le batch
     * @param font La font
     * @param text Le texte à afficher
     * @param rec Le rectangle dans lequel placer le texte
     * @param textAlign Alignement du text dans le rectangle
     * @param truncate Doit-on tronquer le text si il est trop long
     */
    fun drawString(batch: Batch?,
                   font: BitmapFont,
                   text: String,
                   rec: Rectangle,
                   textAlign: TextAlign = TextAlign.CENTER,
                   truncate: Boolean = true) {
        val displayedString = if (truncate) truncatedString(font, text, rec.width) else text
        val layout = GlyphLayout(font, displayedString)

        //Place le text d'une certaine façon selon l'alignement de text choisi
        when (textAlign) {
            TextAlign.UPLEFT -> font.draw(batch,
                    displayedString,
                    rec.x,
                    rec.y + rec.height)

            TextAlign.UP -> font.draw(batch,
                    displayedString,
                    rec.x + rec.width / 2 - layout.width / 2,
                    rec.y + rec.height)

            TextAlign.UPRIGHT -> font.draw(batch,
                    displayedString,
                    rec.x + rec.width - layout.width,
                    rec.y + rec.height)

            TextAlign.CENTERLEFT -> font.draw(batch,
                    displayedString,
                    rec.x,
                    rec.y + rec.height / 2 + layout.height / 2)

            TextAlign.CENTER -> font.draw(batch,
                    displayedString,
                    rec.x + rec.width / 2 - layout.width / 2,
                    rec.y + rec.height / 2 + layout.height / 2)

            TextAlign.CENTERRIGHT -> font.draw(batch,
                    displayedString,
                    rec.x + rec.width - layout.width ,
                    rec.y + rec.height / 2 + layout.height / 2)

            TextAlign.BOTTOMLEFT -> font.draw(batch,
                    displayedString,
                    rec.x,
                    rec.y + layout.height)

            TextAlign.BOTTOM -> font.draw(batch,
                    displayedString,
                    rec.x + rec.width / 2 - layout.width / 2,
                    rec.y + layout.height)

            TextAlign.BOTTOMRIGHT -> font.draw(batch,
                    displayedString,
                    rec.x + rec.width - layout.width,
                    rec.y + layout.height)
        }
    }

    /**
     * Retourne une version tronqué d'une chaîne de caractère selon la taille maximale.
     * Une chaîne tronquée est une version de la chaine où les caractères qui font dépasser
     * la taille maximale sont enlevé, et remplacé par un "..."
     *
     * @param font La font
     * @param text La chaine de caractère
     * @param maxWidth La taille de texte à ne pas dépasser
     * @return La chaîne tronquée
     */
    private fun truncatedString(font: BitmapFont, text: String, maxWidth: Float): String {
        val layout = GlyphLayout(font, text)

        if (layout.width <= maxWidth) return text

        val stringBuilder = StringBuilder()
        for (c in text) {
            val truncatedStringWidth = GlyphLayout(font, "$stringBuilder$c...").width

            if (truncatedStringWidth < maxWidth) {
                stringBuilder.append(c)
            } else {
                stringBuilder.append("...")
                break
            }
        }
        return stringBuilder.toString()
    }

    /** Fonction utilitaire pour creer des fonts */
    fun newFont(fontPath: String, params: FreeTypeFontGenerator.FreeTypeFontParameter.() -> Unit): BitmapFont {

        val generator = FreeTypeFontGenerator(Gdx.files.internal(fontPath))
        val parameter = FreeTypeFontGenerator.FreeTypeFontParameter().apply(params)

        return generator.generateFont(parameter).also { generator.dispose() }
    }
}