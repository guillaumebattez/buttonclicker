package fr.guillaumebattez.buttonclicker.common

import com.badlogic.gdx.assets.AssetManager
import com.badlogic.gdx.audio.Sound
import com.badlogic.gdx.graphics.Texture

object Assets {
    enum class Tex {
        DEBUG,
        BLANK,
        GOLDMINE,
        HITMARKER,
        SHOCKWAVE,
        PICKAXETOP,
        PICKAXEHANDLE,
        TRIANGLE,
        CIRCLE,
        FINGEROUTERWHITE,
        FINGERBASEWHITE,
        CLICKERWHITE,
        MASKCLICKER,
        PAPERFINGER,
        PAPERCLICKER,
        WOODCLICKER,
        WOODFINGER,
        STONECLICKER,
        STONEFINGER,
        IRONCLICKER,
        IRONFINGER,
        GOLDCLICKER,
        GOLDFINGER,
        DIAMONDCLICKER,
        DIAMONDFINGER,
        FIRECLICKER,
        FIREFINGER,
        GOLDNUGGET;

        val path = "textures/${name.toLowerCase()}.png"
        operator fun invoke() : Texture = assetManager.get(path, Texture::class.java)
    }

    enum class Snd {
        MININGHIT,
        CLICKERSPAWN,
        LEVELUP;

        val path = "sounds/${name.toLowerCase()}.ogg"
        operator fun invoke() : Sound = assetManager.get(path, Sound::class.java)
    }

    val assetManager = AssetManager()

    fun dispose() {
        Tex.values().forEach { it().dispose() }
        Snd.values().forEach { it().dispose() }
    }

    fun queueAssets() {
        Tex.values().forEach { assetManager.load(it.path, Texture::class.java) }
        Snd.values().forEach { assetManager.load(it.path, Sound::class.java) }
    }
}