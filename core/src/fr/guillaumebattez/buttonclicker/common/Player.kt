package fr.guillaumebattez.buttonclicker.common

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Vector2
import com.badlogic.gdx.math.Vector3
import fr.guillaumebattez.buttonclicker.entities.*
import fr.guillaumebattez.buttonclicker.extensions.*
import fr.guillaumebattez.buttonclicker.screens.IngameScreen
import java.math.BigInteger
import kotlin.math.roundToLong
import kotlin.properties.Delegates

object Player: Leveling {
    override var level: Int = 0
    override var subLevel: Int = 0

    var score: BigInteger = BigInteger("0")

    val inputCoords = Vector3(Gdx.input.x.toFloat(), Gdx.input.y.toFloat(), 0f)
        get() {
            field.x = Gdx.input.x.toFloat()
            field.y = Gdx.input.y.toFloat()

            Renderer.camera.unproject(field)
            return field
        }
    val inputCoordsV2 = Vector2()
        get() {
            field.x = inputCoords.x
            field.y = inputCoords.y
            return field
        }

    val indexTouchable = arrayOf(true, true, true, true, true, true, true, true, true, true)
    var autoClickTimer = 1f
    var clickPower: BigInteger = BigInteger("1")
        get() = if (level == 0) BigInteger(subLevel.toString()) + BigInteger.ONE else ClickerQuality.powerFromLevel(level, subLevel)/BigInteger("2")

    fun update() {
        for (i in 0 until 10) {
            if (Gdx.input.isTouched(i)) {
                if (!indexTouchable[i]) continue
                if (IngameScreen.goldMine.sprite.boundingRectangle.contains(inputCoordsV2)) {
                    IngameScreen.goldMine.onHit(clickPower, inputCoordsV2, true)
                } else {
                    Assets.Snd.MININGHIT().stop()
                    Assets.Snd.MININGHIT().play()
                    IngameScreen.entities.add(HitmarkerEntity(inputCoordsV2.x, inputCoordsV2.y))
                    Player.earnGold(clickPower, inputCoordsV2, style = FloatingTextEntity.Style.NORMAL)
                    IngameScreen.goldMine.levelProgress += clickPower
                }
                indexTouchable[i] = false
            } else {
                indexTouchable[i] = true
            }
        }

        if (!Gdx.input.isTouched) {
            autoClickTimer = 0.49f
        }
        autoClickTimer -= Gdx.graphics.deltaTime
        if (autoClickTimer <= 0) {
            for (i in 0 until 10) indexTouchable[i] = true
            autoClickTimer = 0.49f
        }
    }

    fun earnGold(amount: BigInteger, pos: Vector2, style: FloatingTextEntity.Style = FloatingTextEntity.Style.ANGLEFROMMINE) {
        score += amount // * BigInteger("20")
        FloatingTextEntity(amount.toCompactString(maxDigitCount = 4), pos, style = style).spawnEntity()
    }
}