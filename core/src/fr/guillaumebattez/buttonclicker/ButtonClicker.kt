package fr.guillaumebattez.buttonclicker

import com.badlogic.gdx.Game
import fr.guillaumebattez.buttonclicker.screens.ScreenLoading

class ButtonClicker : Game() {
    override fun create() {
        setScreen(ScreenLoading(this))
    }
}