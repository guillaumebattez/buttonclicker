package fr.guillaumebattez.buttonclicker.extensions

import com.badlogic.gdx.math.Vector2

fun Vector2.angleFrom(vector: Vector2) = sub(vector).angle()