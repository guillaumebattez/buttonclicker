package fr.guillaumebattez.buttonclicker.extensions

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.Texture
import com.badlogic.gdx.graphics.g2d.SpriteBatch
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.scenes.scene2d.utils.ScissorStack
import fr.guillaumebattez.buttonclicker.common.Renderer

fun SpriteBatch.doWithinBounds(bounds: Rectangle, action: () -> Unit) {
    ScissorStack.calculateScissors(Renderer.camera,
            0f,
            0f,
            Gdx.graphics.width.toFloat(),
            Gdx.graphics.height.toFloat(),
            Renderer.batch.transformMatrix,
            bounds,
            Renderer.recZero)
    ScissorStack.pushScissors(Renderer.recZero)
    action()
    Renderer.batch.flush()
    ScissorStack.popScissors()
}

fun SpriteBatch.draw(texture: Texture, x: Float, y: Float, width: Float, height: Float, color: Color) {
    this.color = color
    this.draw(texture, x, y, width, height)
    this.color = Color.WHITE
}