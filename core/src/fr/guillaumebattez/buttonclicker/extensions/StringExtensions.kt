package fr.guillaumebattez.buttonclicker.extensions

infix fun String.e(i: Int): String {
    val sb = StringBuilder()
    repeat(i) {
        sb.append("0")
    }
    return this + sb.toString()
}