package fr.guillaumebattez.buttonclicker.extensions

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.Vector2
import fr.guillaumebattez.buttonclicker.common.Player

val Sprite.justTouched: Boolean
    get() = boundingRectangle.contains(Player.inputCoords.x, Player.inputCoords.y) && Gdx.input.justTouched()

val Sprite.midX: Float
    get() = x + width/2

val Sprite.endX: Float
    get() = x + width

val Sprite.midY: Float
    get() = y + height/2

val Sprite.endY: Float
    get() = y + height

val Sprite.halfWidth: Float
    get() = width/2

val Sprite.halfHeight: Float
    get() = height/2