package fr.guillaumebattez.buttonclicker.extensions

import com.badlogic.gdx.graphics.Color

val LIGHT_BLUE = Color(0.4f, 0.8f, 1f, 1f)
val PAPER_BASE = Color().apply { set(255, 253, 211) }
val PAPER_OUTLINE = Color().apply { set(170, 147, 94) }
val WOOD_BASE = Color().apply { set(165, 108, 72) }
val WOOD_OUTLINE = Color().apply { set(104, 68, 46) }
val STONE_BASE = Color().apply { set(156, 156, 156) }
val STONE_OUTLINE = Color().apply { set(90, 90, 90) }
val IRON_BASE = Color().apply { set(215, 215, 225) }
val IRON_OUTLINE = Color().apply { set(40, 40, 42) }
val GOLD_BASE = Color().apply { set(255, 217, 100) }
val GOLD_OUTLINE = Color().apply { set(90, 70, 30) }
val DIAMOND_BASE = Color().apply { set(221, 240, 255) }
val DIAMOND_OUTLINE = Color().apply { set(115, 200, 250) }
val REFINED_DIAMOND_BASE = Color().apply { set(50, 170, 255) }
val REFINED_DIAMOND_OUTLINE = Color().apply { set(30, 100, 150) }
val FIRE_BASE = Color().apply { set(255, 253, 236) }
val FIRE_OUTLINE = Color().apply { set(244, 116, 53) }

fun Color.set(r: Int, g: Int, b: Int) {
    this.set(r.toFloat()/255, g.toFloat()/255, b.toFloat()/255, 1f)
}
