package fr.guillaumebattez.buttonclicker.extensions

val Long.digitsNumber: Int
    get() =(Math.log10(this.toDouble()) + 1).toInt()

val Long.Companion.maxIngame: Long
    get() = 9_999_999_999_999_999

val Long.toCompactString: String
    get() = when (this.digitsNumber) {
        in 0..4 -> "$this"
        in 5..7 -> "${this.toString().substring(0..this.digitsNumber - 4)}K"
        in 8..10 -> "${this.toString().substring(0..this.digitsNumber - 7)}M"
        in 11..13 -> "${this.toString().substring(0..this.digitsNumber - 10)}B"
        in 14..16 -> "${this.toString().substring(0..this.digitsNumber - 13)}T"
        else -> "MAX"
    }