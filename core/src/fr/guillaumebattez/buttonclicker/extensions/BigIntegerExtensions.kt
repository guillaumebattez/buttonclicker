package fr.guillaumebattez.buttonclicker.extensions

import java.math.BigInteger

fun BigInteger.digitCount(): Int {
    val factor = Math.log(2.0) / Math.log(10.0)
    val digitCount = (factor * this.bitLength() + 1).toInt()
    if (BigInteger.TEN.pow(digitCount - 1) > this) {
        return digitCount - 1
    }
    return digitCount
}

fun BigInteger.exponentAfterMaxDigitCount(maxDigitCount: Int): Int {
    val step = if (this.digitCount() >  37) 1 else 3
    return ((this.digitCount() - maxDigitCount + 2)/step)*step
}

operator fun BigInteger.times(d: Double): BigInteger = (this.toBigDecimal().times(d.toBigDecimal())).toBigInteger()

fun BigInteger.toCompactString(maxDigitCount: Int) : String {
    val exponent = exponentAfterMaxDigitCount(maxDigitCount)
    val exponentValue = exponent.tenPowValue()
    val value = this.div(exponentValue)
    val exponentSymbol = exponent.tenPowSymbol()

    if (exponentSymbol.contains("e")) {
        val (one, two) = Pair(value.toString().first(), value.toString().last())
        return "$one.$two$exponentSymbol"
    }
    return "$value$exponentSymbol"
}