package fr.guillaumebattez.buttonclicker.extensions

import java.math.BigInteger

fun Int.tenPowValue(): BigInteger = BigInteger.TEN.pow(this.coerceAtLeast(0))

fun Int.tenPowSymbol() = when(this) {
    in 0..2 -> ""
    in 3..5-> "K"
    in 6..8-> "M"
    in 9..11 -> "B"
    in 12..14 -> "T"
    in 15..17 -> "q"
    in 18..20 -> "Q"
    in 21..23 -> "s"
    in 24..26 -> "S"
    in 27..29 -> "O"
    in 30..32 -> "N"
    in 33..35 -> "D"
    else -> "e${this - 1}"
}