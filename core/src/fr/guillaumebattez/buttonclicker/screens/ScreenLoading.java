package fr.guillaumebattez.buttonclicker.screens;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import fr.guillaumebattez.buttonclicker.common.Assets;
import fr.guillaumebattez.buttonclicker.common.Renderer;

/**
 * Created by Guillaume on 04/02/2016.
 */
public class ScreenLoading implements Screen {
    private ShapeRenderer shapeRenderer;
    private float progress;
    Game game;

    public ScreenLoading(Game game)
    {
        this.game = game;
        shapeRenderer = new ShapeRenderer();
        Assets.INSTANCE.queueAssets();
        progress = 0;
    }

    public void update(float delta)
    {
        progress = MathUtils.lerp(progress, Assets.INSTANCE.getAssetManager().getProgress(), 0.2f);
        if(Assets.INSTANCE.getAssetManager().update() && Assets.INSTANCE.getAssetManager().getProgress() - progress < 0.001f)
        {
            game.setScreen(IngameScreen.INSTANCE);
        }
    }

    @Override
    public void show() {

    }

    @Override
    public void render(float delta)
    {
        update(delta);
        shapeRenderer.setProjectionMatrix(Renderer.INSTANCE.getCamera().combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(new Color(0.2f, 0.7f, 0.8f, 1));
        shapeRenderer.rect(0, 0, Renderer.INSTANCE.getCamera().viewportWidth, Renderer.INSTANCE.getCamera().viewportHeight);
        shapeRenderer.setColor(Color.ORANGE);
        shapeRenderer.rect(32, Renderer.INSTANCE.getCamera().viewportHeight / 2 - 32, (Renderer.INSTANCE.getCamera().viewportWidth - 64) * progress, 64);
        shapeRenderer.end();

        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(Color.YELLOW);
        shapeRenderer.rect(32, Renderer.INSTANCE.getCamera().viewportHeight / 2 - 32, Renderer.INSTANCE.getCamera().viewportWidth - 64, 64);
        shapeRenderer.end();
    }

    @Override
    public void resize(int width, int height) {

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose()
    {
        this.shapeRenderer.dispose();
    }
}
