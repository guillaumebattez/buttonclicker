package fr.guillaumebattez.buttonclicker.screens

import com.badlogic.gdx.Gdx
import com.badlogic.gdx.Screen
import com.badlogic.gdx.graphics.Color
import com.badlogic.gdx.graphics.GL20
import com.badlogic.gdx.graphics.g2d.Sprite
import com.badlogic.gdx.math.MathUtils
import com.badlogic.gdx.math.Rectangle
import com.badlogic.gdx.utils.Array
import fr.guillaumebattez.buttonclicker.common.Assets
import fr.guillaumebattez.buttonclicker.common.Font
import fr.guillaumebattez.buttonclicker.common.Player
import fr.guillaumebattez.buttonclicker.common.Renderer
import fr.guillaumebattez.buttonclicker.entities.*
import fr.guillaumebattez.buttonclicker.extensions.*
import fr.guillaumebattez.buttonclicker.views.ScrollView

//TODO: instaciating Rectangle() objects every frames for text: NOT GOOD
object IngameScreen : Screen {
    val clickers = Array<ClickerEntity>()
    val scrollViewHeight = Renderer.V_HEIGHT / 3
    val menuHeight = 70f
    val entities = Array<Entity>()
    val goldMine = GoldEntity()
    val maxMiners = 8
    val scrollView = ScrollView(rec = Rectangle(0f, menuHeight, Renderer.V_WIDTH, scrollViewHeight - menuHeight)).apply {
        repeat(2) {
            children.add(ItemEntity(itemIndex = it, itemType = if (it % 2 == 0) ItemEntity.ItemType.CLICKER else ItemEntity.ItemType.FINGER))
        }
    }
    var clickerToLaunch = 0f
        set(value) {
            field = if (value > maxMiners) {
                entities.filterIsInstance(ClickerEntity::class.java).forEach { it.hasAttacked = false }
                0f
            } else {
                value
            }
        }

    enum class DisplayedSubscreen {
        Ingame, Store
    }

    var displayedSubscreen = DisplayedSubscreen.Store

    init {
        entities.add(goldMine)

        repeat(maxMiners) {
            val circleX = MathUtils.sin((it.toFloat() / maxMiners) * MathUtils.PI2) * goldMine.sprite.width / 1.5f
            val circleY = MathUtils.cos((it.toFloat() / maxMiners) * MathUtils.PI2) * goldMine.sprite.height / 1.5f
            clickers.add(ClickerEntity(launchID = it.toFloat()).apply {
                startingPos.x = circleX + goldMine.sprite.x + goldMine.sprite.halfWidth
                startingPos.y = circleY + goldMine.sprite.y + goldMine.sprite.halfHeight
                spawnEntity()
            })
        }

        displayedSubscreen = DisplayedSubscreen.Ingame
        entities.add(Entity().apply {
            sprite = Sprite(Assets.Tex.DEBUG())
            sprite.setSize(Renderer.V_WIDTH / 2, menuHeight)
            sprite.setPosition(0f, 0f)
            sprite.color = Color.DARK_GRAY
            onTouch = { displayedSubscreen = DisplayedSubscreen.Ingame }
        })
        entities.add(Entity().apply {
            sprite = Sprite(Assets.Tex.DEBUG())
            sprite.setSize(Renderer.V_WIDTH / 2, menuHeight)
            sprite.setPosition(Renderer.V_WIDTH / 2, 0f)
            sprite.color = Color.DARK_GRAY
            onTouch = { displayedSubscreen = DisplayedSubscreen.Store }
        })
    }

    fun update() {
        Player.update()
        scrollView.update()
        clickerToLaunch += Gdx.graphics.deltaTime * 3f
    }

    override fun render(delta: Float) {
        update()

        Gdx.gl.glClearColor(0.3f, 0.25f, 0.25f, 1f)
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT)

        //MAIN BATCH
        Renderer.camera.update()
        Renderer.batch.projectionMatrix = Renderer.camera.combined
        Renderer.batch.begin()

        entities.sortedBy { it.drawPriority }.forEach { it.render() }

        Font.drawString(batch = Renderer.batch,
                font = Font.fatText,
                text = Player.score.toCompactString(4),
                textAlign = Font.TextAlign.UPLEFT,
                rec = Rectangle(20f, 0f, Renderer.V_WIDTH, Renderer.V_HEIGHT - 20))
        scrollView.render()
        Renderer.batch.end()
    }

    override fun resize(width: Int, height: Int) {
        Renderer.viewport.update(width, height)
    }

    override fun dispose() {
        Assets.dispose()
        Renderer.batch.dispose()
    }

    override fun hide() {}
    override fun show() {}
    override fun pause() {}
    override fun resume() {}
}