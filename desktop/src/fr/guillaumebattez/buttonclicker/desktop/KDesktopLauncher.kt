package fr.guillaumebattez.buttonclicker.desktop

import com.badlogic.gdx.backends.lwjgl.LwjglApplication
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration
import fr.guillaumebattez.buttonclicker.ButtonClicker

public class KDesktopLauncher {
    fun main(args : Array<String>) {
        val config = LwjglApplicationConfiguration()
        config.width = 720 / 2
        config.height = 1480 / 2
        LwjglApplication(ButtonClicker(), config)
    }
}