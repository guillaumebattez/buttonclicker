package fr.guillaumebattez.buttonclicker.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import fr.guillaumebattez.buttonclicker.ButtonClicker;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.width = 720/2;
		config.height = 1480/2;
		new LwjglApplication(new ButtonClicker(), config);
	}
}
